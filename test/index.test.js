const proxyquire       = require('proxyquire');
const MyDefaultPrinter = require('../index.js').MyDefaultPrinter;

var printerModule;
var printerStub = {};

// Stubing method printer from module ./printer_module.js
printerStub.printer = (object) => {
  return 'my stub priter working your: '+ object;
};

var index = proxyquire('../index.js', {'./printer_module.js': printerStub});

let result = index.MyDefaultPrinter('HI');
console.log(result);
//my stub priter working your: HI
